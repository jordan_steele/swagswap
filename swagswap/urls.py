from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#Dajaxice
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('imageswap.views',
    url(r'^$', 'home'),
    url(r'^edit/(?P<image_id>\w+)/$', 'home'),
    url(r'^uploadheadshot/$', 'upload_headshot'),
    url(r'^uploadstyle/$', 'upload_style'),
    url(r'^login/$', 'log_in'),
    url(r'^logout/$', 'log_out'),
    url(r'^register/$', 'register'),
    url(r'^account/$', 'account'),
    url(r'^polls/$', 'polls'),
    url(r'^polls/(?P<pk>\d+)/$','poll'),
    url(r'^createpoll/$', 'createpoll'),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
    # Examples:
    # url(r'^$', 'swagswap.views.home', name='home'),
    # url(r'^swagswap/', include('swagswap.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
