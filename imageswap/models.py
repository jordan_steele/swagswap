from django.db import models
from django.contrib.auth.models import User
from swagswap.settings import MEDIA_ROOT

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    last_used_headshot = models.ForeignKey('Headshot', blank=True, null=True)
    votes = models.IntegerField(default=0)
    credibility = models.IntegerField(default=0)

    def __unicode__(self):
        return self.user.username

class Headshot(models.Model):
    image = models.ImageField(upload_to="headshots/")
    user = models.ForeignKey(UserProfile)
    date = models.DateTimeField()

    def __unicode__(self):
        return self.user.user.username

class Style(models.Model):
    image = models.ImageField(upload_to="styles/")
    caption = models.CharField(max_length=50)
    # date = models.DateTimeField()
    # user = models.ForeignKey(UserProfile)

    def __unicode__(self):
        return self.caption

class Image(models.Model):
    date = models.DateTimeField()
    user = models.ForeignKey(UserProfile)
    image = models.ImageField(upload_to="images/")
    style = models.ForeignKey(Style, related_name="style")
    headshot = models.ForeignKey(Headshot, related_name="headshot")
    x_offset = models.IntegerField()
    y_offset = models.IntegerField()
    style_width = models.IntegerField()
    style_height = models.IntegerField()

    def __unicode__(self):
        return self.image.name

    def get_id_as_hex(self):
        return hex(self.id)[2:-1]

class Poll(models.Model):
    user = models.ForeignKey(UserProfile)
    date = models.DateTimeField()
    question = models.CharField(max_length=200)
    is_private = models.BooleanField()
    images = models.ManyToManyField(Image, through='Choice')
    users_voted = models.ManyToManyField(UserProfile, related_name="users_voted", blank=True, null=True)

    def __unicode__(self):
        return self.question

class Choice(models.Model):
    poll = models.ForeignKey(Poll)
    image = models.ForeignKey(Image)
    votes = models.IntegerField()
    
    def __unicode__(self):
        return self.poll.question