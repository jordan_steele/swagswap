"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from imageswap.models import Choice

class ChoiceTest(TestCase):
	def setUp(self):
		self.choice = Choice.objects.create(poll=1, image=2, votes=38)
	def test_votes_correct(self):
		self.assertEqual(self.choice.votes, 38)


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)
