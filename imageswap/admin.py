from imageswap.models import UserProfile, Headshot, Style, Image, Poll, Choice
from django.contrib import admin

admin.site.register(UserProfile)
admin.site.register(Headshot)
admin.site.register(Style)
admin.site.register(Image)
admin.site.register(Poll)
admin.site.register(Choice)
