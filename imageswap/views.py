import Image as PIL
import os, uuid
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import logout, authenticate, login
from swagswap.settings import MEDIA_ROOT
from imageswap.models import UserProfile, Headshot, Style, Image, Poll, Choice
from imageswap.forms import RegisterForm, LoginForm, CreatePollQuestionForm, CreatePollPrivateForm


def home(request, image_id=None):
    #Gets a completed image for editing if a image id is entered
    headshot = None
    if (image_id):
        image = Image.objects.get(pk=int(image_id, 16))
    else:
        image = None
        #Gets the last used headshot of the user if they have one
        if request.user.is_authenticated() and request.user.get_profile().last_used_headshot != None:
            headshot = request.user.get_profile().last_used_headshot

    #Gets style objects
    styles = Style.objects.all()#.order_by('-date')

    if request.user.is_authenticated():
        user_headshots = Headshot.objects.filter(user=request.user.get_profile()).order_by('-date')
    else:
        user_headshots = None

    return render_to_response('imageswap/home.html', 
                              {'loginForm': LoginForm,
                               'styles': styles,
                               'headshot': headshot,
                               'image': image,
                               'userHeadshots': user_headshots},
                              context_instance=RequestContext(request))

@login_required
def upload_headshot(request):
    CONTENT_TYPES = ['image']
    ALLOWED_EXTENSIONS = ['.jpeg', '.jpg', '.jpe', '.png']
    EXTENSIONS_NOT_CONVERTED = ['.jpeg', '.jpg', '.jpe']
    MAX_UPLOAD_SIZE = 5242880 #5mb

    if request.method == 'POST':
        image = request.FILES['uploaded-headshot']  
        extension = os.path.splitext(image.name)[1]
        content_type = image.content_type.split('/')[0]
        #Check to see if image is appropriate for saving
        if extension in ALLOWED_EXTENSIONS and image._size <= MAX_UPLOAD_SIZE and content_type in CONTENT_TYPES:
            #Create required filenames for the new image
            uid = str(uuid.uuid4())[:15]
            filename = uid + "_swagswap" + extension
            relative_filename = os.path.join('headshots', filename)
            absolute_filename = os.path.join(MEDIA_ROOT, relative_filename)

            #Save the image on the file system
            destination = open(absolute_filename, 'wb+')
            for chunk in image:
                destination.write(chunk)
            destination.close()

            #Crop sides of images that are not a portrait
            cropped_image = PIL.open(absolute_filename, 'r')
            if (float(cropped_image.size[1]) / cropped_image.size[0]) < 1.25:
                desired_width = (cropped_image.size[1] / 1.33)
                width_difference = cropped_image.size[0] - desired_width
                cropped_image.crop((int(width_difference/2), 0, int(width_difference/2 + desired_width), cropped_image.size[1])).save(absolute_filename)

            #Convert images that made it through that aren't jpegs, to jpegs
            if extension not in EXTENSIONS_NOT_CONVERTED:               
                image_to_be_converted = PIL.open(absolute_filename, 'r')
                to_be_deleted = absolute_filename
                extension = '.jpeg'
                filename = uid + "_swagswap" + extension
                relative_filename = os.path.join('headshots', filename)
                absolute_filename = os.path.join(MEDIA_ROOT, relative_filename)
                image_to_be_converted.save(absolute_filename)
                #os.remove(to_be_deleted)

            
            #Save image in database
            user_profile = request.user.get_profile() 
            new_headshot = Headshot(image=relative_filename,
                                    user=user_profile,
                                    date=datetime.now())
            new_headshot.save()
            #Set the last used headshot for the user to the new one
            user_profile.last_used_headshot = new_headshot
            user_profile.save()

    return HttpResponseRedirect(reverse('imageswap.views.home', args=()))

@login_required
def upload_style(request):
    CONTENT_TYPES = ['image']
    ALLOWED_EXTENSIONS = ['.png']
    MAX_UPLOAD_SIZE = 5242880 #5mb

    if request.method == 'POST':
        image = request.FILES['uploaded-style']  
        extension = os.path.splitext(image.name)[1]
        content_type = image.content_type.split('/')[0]
        #Check to see if image is appropriate for saving
        if extension in ALLOWED_EXTENSIONS and image._size <= MAX_UPLOAD_SIZE and content_type in CONTENT_TYPES:
            #Create required filenames for the new image
            uid = str(uuid.uuid4())[:15]
            filename = uid + "_swagswap" + extension
            relative_filename = os.path.join('styles', filename)
            absolute_filename = os.path.join(MEDIA_ROOT, relative_filename)

            #Save the image on the file system
            destination = open(absolute_filename, 'wb+')
            for chunk in image:
                destination.write(chunk)
            destination.close()
            
            #Save image in database
            new_style = Style(image=relative_filename,caption="hair")
            new_style.save()

    return HttpResponseRedirect(reverse('imageswap.views.home', args=()))

def log_in(request):
    return render_to_response('imageswap/login.html',
                              {'loginForm': LoginForm},
                              context_instance=RequestContext(request))

def log_out(request):
    logout(request);
    return HttpResponseRedirect(reverse('imageswap.views.home', args=()))

def register(request):
    form = RegisterForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            try:
                old_user = User.objects.get(username=form.cleaned_data['username'])
            except User.DoesNotExist:
                #new_user = User(username=form.cleaned_data['username'],
                #                password=form.cleaned_data['password'])
                new_user = User.objects.create_user(form.cleaned_data['username'], '', form.cleaned_data['password'])
                new_user.set_password(form.cleaned_data['password'])
                new_user.save()
                UserProfile.objects.create(user=new_user)

                #log new user in
                user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
                if user is not None and user.is_active:
                    login(request, user)

                return HttpResponseRedirect(reverse('imageswap.views.home', args=()))
    
    return render_to_response('imageswap/register.html', 
                              {'form': form,
                              'loginForm': LoginForm},
                              context_instance=RequestContext(request))

@login_required
def account(request):
    polls = Poll.objects.filter(user=request.user.get_profile()).order_by('-date')
    #extra_polls = 5 - polls.count
    images = Image.objects.filter(user=request.user.get_profile()).order_by('-date')
    return render_to_response('imageswap/account.html',
                              {'polls': polls,
                               #'extra_polls': extra_polls,
                              'images': images},
                              context_instance=RequestContext(request))

def polls(request):
  display_num = 3 #the number of polls to display on a page
  older_button = True #if the older button will be showed
  polls = Poll.objects.filter(is_private=False).order_by('-date')

  #hide back button
  if len(polls) <= display_num:
    older_button = False

  #create JSON with the each poll, and the number of empty slots to fill 
  polls = polls[:display_num]
  polls_set = []
  for poll in polls:
    polls_set.append({'poll':poll,
      'remainder': 'banana'})


  display_num -= 1


  

  return render_to_response('imageswap/polls.html',{
                              'polls': polls,
                              'loginForm': LoginForm,
                              'display_num':display_num,
                              'back':older_button,
                 
                              },context_instance = RequestContext(request))



def poll(request, pk):
  #get choices and poll to display
  choices = Choice.objects.filter(poll=pk)
  poll = Poll.objects.get(pk=pk)

  return render_to_response('imageswap/poll.html',
                            {'choices':choices,
                             'poll':poll,
                             'loginForm': LoginForm},
                            context_instance = RequestContext(request))

@login_required
def createpoll(request):
  #get all the user images to choose from
  images = Image.objects.filter(user=request.user.get_profile()).order_by('-date')



  return render_to_response('imageswap/createpoll.html',
                             {'images':images,
                             'CreatePollQuestionForm':CreatePollQuestionForm,
                             'CreatePollPrivateForm':CreatePollPrivateForm},
                             context_instance = RequestContext(request))