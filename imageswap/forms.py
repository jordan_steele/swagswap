from django import forms
from captcha.fields import ReCaptchaField

class RegisterForm(forms.Form):
    username = forms.CharField(max_length=20, min_length=3)
    password = forms.CharField(max_length=32, min_length=5, widget=forms.PasswordInput)
    captcha = ReCaptchaField()

class LoginForm(forms.Form):
    username = forms.CharField(max_length=20, min_length=3)
    password = forms.CharField(max_length=32, min_length=5, widget=forms.PasswordInput)

class CreatePollQuestionForm(forms.Form):
	question = forms.CharField(max_length=200, label='Poll Question', initial="Write your question...")
 	

class CreatePollPrivateForm(forms.Form):
 	private = forms.BooleanField(initial=False,required=False,label='Would you like this poll to be private?')