import Image as PIL
import uuid, os
from datetime import datetime
from django.http import HttpResponse
from django.utils import simplejson
from dajaxice.decorators import dajaxice_register
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from swagswap.settings import MEDIA_ROOT
from imageswap.models import UserProfile, Headshot, Style, Image, Poll, Choice
from django.utils.timezone import utc
from django.db.models import F
from django.db import transaction

@dajaxice_register
def log_in(request,username,password):
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            loggedIn = True
            userName = user.username
        else:
            loggedIn = False
            userName = None
    else:
        loggedIn = False
        userName = None
    return simplejson.dumps({'loggedIn':loggedIn, 
			     'userName':userName})

@dajaxice_register
@login_required
def save_image(request, headshot_id, x_offset, y_offset, headshot_width, headshot_height, style_width, style_height, style_id):
    if style_id == 0:
        return simplejson.dumps({'completed': False})
    #Get headshot and image objects
    headshot = Headshot.objects.get(pk=headshot_id)
    style = Style.objects.get(pk=style_id)
    
    #Get the headshot and image files
    headshot_image = PIL.open(os.path.join(MEDIA_ROOT, headshot.image.name), 'r')
    style_image = PIL.open(os.path.join(MEDIA_ROOT, style.image.name), 'r')

    #Adjust values to suit the full sized images in the backend
    x_offset = (headshot_image.size[0] * x_offset) / headshot_width
    y_offset = (headshot_image.size[1] * y_offset) / headshot_height
    style_width = (headshot_image.size[0] * style_width) / headshot_width
    style_height = (headshot_image.size[1] * style_height) / headshot_height

    #Create thumbnail of image in filesystem to suit needs
    style_image = style_image.resize((style_width, style_height), PIL.ANTIALIAS)

    #Overlay the style on the headshot
    headshot_image.paste(style_image, (x_offset, y_offset), style_image)

    #Save image in file system
    uid = str(uuid.uuid4())[:15]
    filename = uid + "_swagswap.jpg"
    relative_filename = os.path.join('images', filename)
    absolute_filename = os.path.join(MEDIA_ROOT, relative_filename)
    headshot_image.save(absolute_filename)

    #Create database entry
    new_image = Image(pk=int(str(uuid.uuid4().int)[:15]),
                      date=datetime.now(),
                      user=request.user.get_profile(),
                      image=relative_filename,
                      style=style,
                      headshot=headshot,
                      x_offset=x_offset,
                      y_offset=y_offset,
                      style_width=style_width,
                      style_height=style_height)
    new_image.save()

    return simplejson.dumps({'completed': True})

@login_required
@dajaxice_register
def update_last_headshot(request, headshot_id):
    user_profile = request.user.get_profile()
    user_profile.last_used_headshot = Headshot.objects.get(pk=headshot_id)
    user_profile.save()

@login_required
@dajaxice_register
def delete_image(request, image_id):
    i = Image.objects.get(pk=image_id)
    i.poll_set.all().delete() 
    i.delete()

    return simplejson.dumps({'image_id': image_id})

@transaction.commit_on_success
@dajaxice_register
def vote(request,choicepk):
    choice = Choice.objects.get(pk=choicepk)
    choice.save()

    blocked = False
    if 'voted' in request.session:
  
        # FOUND A COOKIE
        if choice.poll in request.session['voted']:
            # ALREADY VOTED
            blocked = True
        else:
            # ALLOWED
            tmp = request.session['voted']
            tmp.append(choice.poll)
            request.session['voted'] = tmp
            choice.votes += 1

            choice.save()
    else:
        # DID NOT FIND COOKIE
        request.session['voted'] = [choice.poll]
        choice.votes += 1
        choice.save()

# choice.votes = F('votes') + 1
#             choice.save()

    return simplejson.dumps({'choicepk':choicepk,'votes':choice.votes,
                                                 'blocked':blocked})

@dajaxice_register
def create_poll(request,question,private,images):

    if(question == ""):
        question = "Which one?"
    #create new entry
    new_poll = Poll(user = request.user.get_profile(),
                    date = datetime.utcnow().replace(tzinfo=utc),
                    question = question,
                    is_private = private)

    

    dbimages = []
    # add each selected image
    for imagepk in images.values():
        imagepk = int(imagepk)
        if(imagepk != -1):
            image = Image.objects.get(pk=imagepk)
            dbimages.append(image)    


    # you can't have a poll with one image
    if(len(dbimages) < 2):
        invalid_poll=True

    else:
        invalid_poll=False

       #save data
        new_poll.save()

        for image in dbimages :
            choice = Choice(poll = new_poll,   image =image, votes = 0)
            choice.save()


 
    #return HttpResponseRedirect(reverse('imageswap.views.home', args=()))
    return simplejson.dumps({   'fail':invalid_poll, 'pollpk':new_poll.pk})

@dajaxice_register
def polls_set(request,newest,back,display_num,url):
    
    # must increment displaynum because indexing method
    display_num+=1
    #get the dat of the item at the top of the page
    date = Poll.objects.get(pk = newest).date

    #if browsing older polls
    if back:
        polls = Poll.objects.filter(is_private=False, date__lt=date).order_by('-date')
        print len(polls), display_num
        #check if we should remove button to go further back
        try:
            flag=False
            polls[display_num*2]
        
        except:
            print "sdlfkjasd"
            older_button = False
            flag=True

        polls = polls[display_num-1:display_num*2-1]
        #removes corresponding button
        if(len(polls) < 3 or flag):

            older_button = False
            newer_button = True
        else:
            older_button = True
            newer_button = True
    #if browsing newer polls
    else:
        polls = Poll.objects.filter(is_private=False, date__gt=date).order_by('-date')
        newest_poll = polls[0]
        polls = polls[len(polls)-display_num:len(polls)]
        #remove unecesary buttons
        if(len(polls) < 3 or polls[0] == newest_poll):
            newer_button = False
            older_button = True
        else:
            newer_button = True
            older_button = True
        
    #now we have new poll set, create JSON data
    poll_and_images = []
    for i in range(len(polls)):
        poll_and_images.append({})

        poll_and_images[i]['question']=polls[i].question
        poll_and_images[i]['poll']=polls[i].pk
        poll_and_images[i]['user']=UserProfile.objects.get(pk=polls[i].user.pk).user.username
    
        poll_and_images[i]['images']=[]

        choices = Choice.objects.filter(poll=polls[i])

        for choice in choices:
            poll_and_images[i]['images'].append(choice.image.image.url)

    return simplejson.dumps({
                    'polls': poll_and_images,
                    'url':url,
                    'display_num': display_num,
                    'older_button' :older_button,
                    'newer_button':newer_button
                    })
                            