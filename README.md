A Django web application for trying on hairstyles and sharing with friends or the public to gauge opinion. 

![ScreenShot](https://bitbucket.org/jordan_steele/swagswap/raw/68d72267d83cbd6a12eee0ea734423ff29608d58/screenshots/nostyle.png)
![ScreenShot](https://bitbucket.org/jordan_steele/swagswap/raw/68d72267d83cbd6a12eee0ea734423ff29608d58/screenshots/withstyle.png)
