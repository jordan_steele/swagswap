function delete_image_callback(data){
    $('#account-image-' + data.image_id).parent().remove();
    $('#image-count').text(parseInt($('#image-count').text()) - 1);
}

function updateHeight()
{
    $('.account-image-container').height($('.account-image-container').width() * 1.33);
}

// function updateImages()
// {
// 	$('.account-image').load(function(){
// 		var maxWidth = $(this).parent().width();
// 		var maxHeight = $(this).parent().width();

// 		if ($(this).height() > maxHeight){
// 			$(this).height(maxHeight);
// 		}
// 	})
// }

$(window).resize(function(){
	updateHeight();
});

$(document).ready(function(){
	updateHeight();

	$('#polls-selector').click(function() {
		$('#polls-selector').addClass('selected');
		$('#polls-selector').removeClass('not-selected');
		$('#account-polls').removeClass('hidden');
		$('#account-polls').addClass('unhidden-account');
		if ($('#account-images').hasClass('unhidden-account')){
			$('#images-selector').removeClass('selected');
			$('#images-selector').addClass('not-selected');
			$('#account-images').addClass('hidden');	
			$('#account-images').removeClass('unhidden-account');
		}
	});

	$('#images-selector').click(function() {
		$('#images-selector').addClass('selected');
		$('#images-selector').removeClass('not-selected');
		$('#account-images').removeClass('hidden');
		$('#account-images').addClass('unhidden-account');
		if ($('#account-polls').hasClass('unhidden-account')){
			$('#polls-selector').removeClass('selected');
			$('#polls-selector').addClass('not-selected');
			$('#account-polls').addClass('hidden');	
			$('#account-polls').removeClass('unhidden-account');
		}
		updateHeight();
	});

	$('.account-image-container').hover(function() {
		$(this).find('.delete-image-image').stop(false,true).css('opacity', '1');
		$(this).find('.edit-image-image').stop(false,true).css('opacity', '1');
	},
	function() {
		$(this).find('.delete-image-image').stop(false,true).css('opacity', '0');
		$(this).find('.edit-image-image').stop(false,true).css('opacity', '0');
	});
});