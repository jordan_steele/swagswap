function vote_callback(data){

    //get tag of element needed to be changed
    var str1 = "#choice";
    var n = str1.concat(data.choicepk);
   
    //update numbers
    $(n).text(data.votes);
    $(n).attr('value',data.votes);


    var maxvotes = 0; //the highest vote (used to colour winning vote yellow)
    var totalvotes = 0; //used for percentages on poll
    var votes = new Array();
    var vote;
    var index; //highest image index
    

    $('.poll-votes').each(function(i) {
    	vote = parseInt($(this).attr("value"));
        //find highest vote count
    	if(vote > maxvotes) {
    		maxvotes = vote;
    		index = i;
    	}
        totalvotes += vote;
    	
    	votes[i] = $(this).text();

    	

    });

    //if there are no votes there, it means you just voted
    if(totalvotes == 0)
        totalvotes = 1;

    //update progress bar percentages
    $('.progressbar').each(function(i) {      
    	$(this).attr("data-perc",votes[i]/totalvotes*100);
    
    });

    //change colour of winning bar
    var colorchange = $('.bar').eq(index);
    colorchange.toggleClass('color3');
    colorchange.attr('class',"bar color2");
    $('.poll-votes').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0});

    //alert user if they have already voted
    if(data.blocked) {
    	$('#poll-error').text("You have already voted");
        $('#poll-error').fadeIn(2000);
    	
    }

    //progress bar animation
    $('.progressbar').each(function(i){
        var barlength = 1.34 * $(this).width()/124; // length of bar control
     
		var t = $(this),
		dataperc = t.attr('data-perc'), //full percentage
		barperc = Math.round(dataperc*barlength); //real time size
		t.find('.bar').animate({width:barperc}, dataperc*30);

		
		function perc() {
			var length = t.find('.bar').css('width'),
				perc = Math.round(parseInt(length)/barlength),
				labelpos = (parseInt(length)-2);

		
			t.find('.perc').text(perc+'%');
		}
		perc();
		setInterval(perc, 0); 
	});

}

