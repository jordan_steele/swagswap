function login_callback(data)
{
    if (data.loggedIn){
        var vars = [], hash;
        var q = document.URL.split('?')[1];
        if(q != undefined){
            q = q.split('&');
            for(var i = 0; i < q.length; i++){
                hash = q[i].split('=');
                vars.push(hash[1]);
                vars[hash[0]] = hash[1];
            }
        }

        if (vars['next'] != null ){
            window.location.replace(window.location.origin + vars['next']);
        }
        if ($('#account-container').length == 0){
            $('#login-container').remove();
            $('#navigation-bar').append('<div id="account-container"><form action="' + logoutURL + '"><button id="logout-button" class="nav-bar-links nav-bar-links-right" type="submit">Logout</button></form><a id="account-link" class="nav-bar-links nav-bar-links-right" href="' + accountURL + '">' + data.userName + '</a></div>');
        }
    }
    else {
        if ($('#login-form-container .error-message').length == 0){
            $('#login-form-container').prepend("<div class='error-message'>Login failed.</div>");
        }
    }
}

$(document).ready(function(){
    $('#login-popup-button').click(function(){
        $('#login-form-container').toggleClass('hidden');
    });
});