function save_image_callback(data)
{

}

function update_last_headshot_callback(data)
{

}

function make_active_style_resizable_draggable(){
    $( ".active-style ").resizable({containment: "#workspace",
        create: function(e, ui) {
            var parent = $(this).parent();
            var child = $(this).children(":first");
            $(this).css({
                width: parseInt($(this).css('width'))/parseInt(parent.css('width'))*100+"%",
                height: parseInt($(this).css('height'))/parseInt(parent.css('height'))*100+"%",
                left: parseInt($(this).css('left'))/parseInt(parent.css('width'))*100+"%",
                top: parseInt($(this).css('top'))/parseInt(parent.css('height'))*100+"%"
            });
            child.css({
                width: '100%',
                height: '100%'
            });
        },
        stop: function(e, ui) {
            var parent = ui.element.parent();
            var child = ui.element.children(":first");
            ui.element.css({
                width: ui.element.width()/parent.width()*100+"%",
                height: ui.element.height()/parent.height()*100+"%",
                left: parseInt(ui.element.css('left'))/parseInt(parent.width())*100+"%",
                top: parseInt(ui.element.css('top'))/parseInt(parent.height())*100+"%"
            });
            child.css({
                width: '100%',
                height: '100%'
            });

        }
    }).parent().draggable({containment: "#workspace", 
        cancel: null,
        stop: function() {
            var parent = $(this).parent();
            $(this).css({
                    left: parseInt($(this).css('left'))/parseInt(parent.css('width'))*100+"%",
                    top: parseInt($(this).css('top'))/parseInt(parent.css('height'))*100+"%"
            });
        }
    });
}

$(document).ready(function() {
    //If the user has selected to edit a image made previously
    if (typeof styleID !== "undefined"){
        $("#headshot").load(function() {
            //Add the style image they had useds
            var new_style_image = $('<input type="image" class="active-style" src="' + styleURL + '" value="' + styleID + '"/>');
            $( "#workspace" ).append(new_style_image);

            //Set it's dimensions and position
            $(new_style_image).css("width", parseInt($("#headshot").css("width")) * (style_width / headshot_width));
            $(new_style_image).css("height", parseInt($("#headshot").css("height")) * (style_height / headshot_height));
            $(new_style_image).css("left", parseInt($("#headshot").css("width")) * (x_offset / headshot_width)
                                     + (parseInt($('#workspace').css('width')) - parseInt($('#headshot').css('width')))/2);
            $(new_style_image).css("top", parseInt($("#headshot").css("height")) * (y_offset / headshot_height) 
                                     + parseInt($('#workspace').css('height')) - parseInt($('#headshot').css('height')));

            //Make it resizable and draggable
            make_active_style_resizable_draggable();
        }).each(function() {
            if (this.complete) $(this).load();
        });
    }

    $( ".style" ).click(function() {
        if ($( ".active-style" ).length == 0){
            $( "#workspace" ).append($(this).clone().removeClass("style").addClass("active-style"));
            $(this).addClass("inactive-style");
        }
        else {
            $( ".active-style" ).attr('src', $(this).attr('src')).attr('value', $(this).attr('value'));   
            $( ".inactive-style" ).removeClass("inactive-style");
            $(this).addClass("inactive-style");
        }
        make_active_style_resizable_draggable();
    });

    $('.fancybox').fancybox({
        autoSize: false,
        autoHeight: true,
        width: '60%'
    });

    $('#upload-headshot-button').bind("click" , function () {
        $('#upload-headshot').click();
    });

    $('#upload-headshot').change(function() {
        $('#upload-headshot-form').submit();
    });

    $('#upload-style-button').bind("click" , function () {
        $('#upload-style').click();
    });

    $('#upload-style').change(function() {
        $('#upload-style-form').submit();
    });

    $('.user-headshots').click(function(){
        var old_headshot_src = $('#headshot').attr('src');
        var old_headshot_val = $('#headshot').val();
        $('#headshot').attr('src', $(this).attr('src'));
        $('#headshot').val($(this).val());
        $(this).attr('src', old_headshot_src);
        $(this).val(old_headshot_val);
        $.fancybox.close();
        Dajaxice.imageswap.update_last_headshot(update_last_headshot_callback, {'headshot_id':$('#headshot').val()})
    });

    $('#save-button').click(function(){
        if ($('.active-style').length != 0){
            var style_id = $('.active-style').val();
        }
        else {
            var style_id = 0;
        }
        Dajaxice.imageswap.save_image
                (save_image_callback, {'headshot_id':$('#headshot').val(),
                                  'x_offset':parseInt($('.active-style').parent().css('left')) - (parseInt($('#workspace').css('width')) - parseInt($('#headshot').css('width')))/2,
                                  'y_offset':parseInt($('.active-style').parent().css('top')) - (parseInt($('#workspace').css('height')) - parseInt($('#headshot').css('height'))),
                                  'headshot_width':parseInt($('#headshot').css('width')),
                                  'headshot_height':parseInt($('#headshot').css('height')),
                                  'style_width':parseInt($('.active-style').parent().css('width')),
                                  'style_height':parseInt($('.active-style').parent().css('height')),
                                  'style_id':style_id});
    })
});