function nextpoll_callback(data) {

	

	for(var i = 0;i<data.polls.length;i++){

		var images =$(".polls-images").eq(i).children();
		//swap images to the next set
		for(var j = 0;j<5;j++) {			
			if(j<data.polls[i].images.length) {	
				
				$(images[j]).attr("src", data.polls[i].images[j]);
				$(images[j]).eq(j).show();
			}
			else {				
				$(images[j]).eq(j).hide();
			}
		}

		//update question and url to current poll
		$(".polls-question").eq(i).text(data.polls[i].question);
		var url = data.url + data.polls[i].poll.toString();
		$(".polls-question").eq(i).attr('href',url);	
	}

	//update poll values after all images have been changed
	for(var i = 0;i<data.display_num;i++){
		if(i<data.polls.length) {
			$(".polls-curr").eq(i).attr('value',data.polls[i].poll);
			$(".polls-curr").eq(i).show();
		}
		else {
			$(".polls-curr").eq(i).attr('value',"null");
			$(".polls-curr").eq(i).hide();
		}

	}

	$('.polls-image').each(function(i){
		$(this).addClass('light-background-border');

		
	})

	//hide navigation buttons
	if(data.older_button) 
		{
		$('#polls-older').show();
	}
	else
		{
		$("#polls-older").hide();

	}
	if(data.newer_button) 
		$("#polls-newer").show();
	else
		$("#polls-newer").hide();





}